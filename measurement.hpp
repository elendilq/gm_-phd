#pragma once
#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <ctime>
#include "target_form.hpp"

using namespace std;
// 定义一个结构体来保存测量数据
struct Measurement
{
    Eigen::MatrixXd m; // 量测结果
    int n;
    Eigen::Vector2d s;
    Eigen::MatrixXd c;
    Eigen::MatrixXd v;
};

inline Eigen::VectorXd TSubV(std::vector<Eigen::MatrixXd> GT, Eigen::VectorXd Sensor, int measure_id, int target_id)
{

    Eigen::VectorXd rela_pos(6);
    for (int i = 0; i < 6; i++)
    {
        rela_pos[i] = GT[target_id](i, measure_id) - Sensor[i];
    }
    return rela_pos;
};

/**
 * @brief 模拟测量函数
 *
 * @param H 状态转移矩阵
 * @param r_noise 测量噪声
 * @param target_position 真实位置 m.s [6,mea_num,target_num]
 * @param N 测量次数
 * @param pd 目标检测概率
 * @return std::vector<Measurement>
 */
std::vector<Measurement> MEASUREMENT(const Eigen::Matrix<double, 3, 6>& H, const Eigen::Vector3d& r_noise,
    Real_State& target_position, int N,
    double pd,int r=3)
{
    Eigen::VectorXd sensor_position(6);
    sensor_position << 0, 0, 0, 0, 0, 0;
    double a = -1000;
    double b = 1000;
    std::vector<Measurement> Y(N);

    // 初始化测量数据
    for (int i = 0; i < N; ++i)
    {
        Y[i].m.resize(3, 0);
        Y[i].n = 0;
        Y[i].s.setZero();
    }

    // 测量杂波
    std::default_random_engine generator;
    std::poisson_distribution<int> distribution(r);
    std::uniform_real_distribution<double> uniform(0.0, 1.0);
    for (int i = 0; i < N; ++i)
    {
        int clutter = distribution(generator);
        for (int j = 0; j < clutter; ++j)
        {
            Eigen::VectorXd y(3);
            for (int i = 0; i < 3; ++i) {
                y(i) = uniform(generator);
            }
            y = (y.array() * (b - a) + a).matrix();
            Y[i].m.conservativeResize(Y[i].m.rows(), Y[i].m.cols() + 1);
            Y[i].m.col(Y[i].m.cols() - 1) = y;
        }
    }
    
    // 测量目标状态
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < 1; ++j)
        {
            if (target_position.n(j, i) == 1)
            {
                double s = uniform(generator);
                if (s < pd)
                {
                    Eigen::Vector3d vec1;

                    for (int k = 0; k < 3; k++)
                    {
                        vec1[k] = uniform(generator);
                    }
                    Eigen::VectorXd rela_pos = TSubV(target_position.m.s, sensor_position, i, j);
                    Eigen::Vector3d y = H * rela_pos + r_noise.cwiseProduct(vec1);
                    Y[i].m.conservativeResize(Y[i].m.rows(), Y[i].m.cols() + 1);                    
                    Y[i].m.col(Y[i].m.cols() - 1) = y;
                    
                }
            }
        }
        //cout << Y[i].m << endl;
        Y[i].n = Y[i].m.cols();
    }

    return Y;
}