#include <Eigen/Dense>
#include <vector>
#include <cmath>
#include <math.h>
#include<numeric>
#include <iostream>
#include <fstream>
#include "target_form.hpp"
#include "measurement.hpp"

using namespace std;

// class State
// {
// public:
//     std::vector<Eigen::MatrixXd> m; // [target_num,6,mea_num]
//     Eigen::VectorXd w;
//     std::vector<Eigen::MatrixXd> P; // vector(P1,P2,...)
//     double J;                       // 目标数
//     State::State(int mea_num, int target_num)
//     {
//         for (int i = 0; i < target_num; i++)
//         {
//             Eigen::MatrixXd matrix(6, mea_num);
//             matrix.setConstant(0.0);
//             m.push_back(matrix);
//         }

//         w.resize(target_num);
//         w.setConstant(1.0);
//     }
// };

struct State
{
    std::vector<Eigen::MatrixXd> m; // [mea_num,(6,1)]
    std::vector<double> w; // [mea_num] 默认了target_num=1,要改
    std::vector<Eigen::MatrixXd> P; // vector(P1,P2,...)
    int J;
    State()
    {
		J = 0;
	}
};


inline Eigen::MatrixXd concat(Eigen::MatrixXd& a, Eigen::MatrixXd& b)
{
	Eigen::MatrixXd c(a.rows(), a.cols() + b.cols());
	c << a, b;
	return c;
}



struct RandomState // 意义不明的重复构造
{
    std::vector<Eigen::MatrixXd> m; // 状态[sensor_num,6,target_num]
};

struct FilterPHD
{
    std::vector<State> a; //[sensor_num]
    FilterPHD(void)
    {
		a.clear();
	}
    FilterPHD(const FilterPHD& other) {
        // 深拷贝
        a = other.a;
    }
};

/**
 * @brief 模拟目标状态生成
 *
 * @param F 目标状态转移矩阵
 * @param G 状态噪声转移矩阵
 * @param q_noise 状态噪声方差
 * @return std::vector<TargetPosition>
 */
Real_State TARGET_FORM(Eigen::MatrixXd& F, Eigen::MatrixXd& G, Eigen::VectorXd& q_noise)
{

    // // 初始化目标状态
    // target_position.m.resize(1, 0);
    // target_position.n.resize(1, 100);

    // 设置初始状态
    int target_num = 1;
    int measure_num = 100;
    Real_State s0(target_num, measure_num);
    Eigen::VectorXd u(6);        // 假设状态向量的维度为6
    u << 200, -1, 200, -1, 0, 0; // 初始状态值
    for (int i = 0; i < 6; i++)
    {
        s0.m.s[0](i, 0) = u(i);
    }
    s0.n(0, 0) = 1;

    // 模拟状态随时间的变化
    for (int i = 1; i < 100; ++i)
    {
        u = F * u + G * (q_noise.asDiagonal() * Eigen::VectorXd::Random(3));
        for (int j = 0; j < 6; j++)
        {
            s0.m.s[0](j, i) = u(j);
        }
        s0.n(0, i) = 1;
    }
    /*cout << s0.m.s[0].col(0) << endl;
    cout << s0.m.s[0].col(1) << endl;
    cout << s0.m.s[0].col(2) << endl;
    cout << s0.m.s[0].col(3) << endl;*/
    return s0;
}

State PREDICT(State x_filter, Eigen::MatrixXd& F, double ps, State birth, Eigen::VectorXd& q_noise, Eigen::MatrixXd& G, int target_id = 0)
{
    // 新生目标预测
    State x_birth = birth;

    // 存活目标预测
    Eigen::MatrixXd q = q_noise.asDiagonal();
    State x_survival;
    x_survival.m.clear();
    x_survival.J = x_filter.J;
    for (int k = 0; k < x_filter.J; k++)
    {
        Eigen::MatrixXd x = F * x_filter.m[k];
        double w = ps * x_filter.w[k];
        Eigen::MatrixXd P = F * x_filter.P[k] * F.transpose() + G * q * G.transpose();
        x_survival.m.push_back(x);
        x_survival.P.push_back(P);
        x_survival.w.push_back(w);
    }
    State x_predict;
    x_predict.m = x_birth.m;
    x_predict.m.insert(x_predict.m.end(), x_survival.m.begin(), x_survival.m.end());
    x_predict.P = x_birth.P;
    x_predict.P.insert(x_predict.P.end(), x_survival.P.begin(), x_survival.P.end());
    x_predict.w = x_birth.w;
    x_predict.w.insert(x_predict.w.end(), x_survival.w.begin(), x_survival.w.end());
    x_predict.J = x_birth.J + x_survival.J;
    return x_predict;
};


State UPDATE(State x_predict, Measurement Y, double pd, Eigen::MatrixXd& H, Eigen::MatrixXd& R, double r,int target_id=0)
{
    // 对漏检目标更新
    std::vector<double> w = x_predict.w;
    for (auto& wi : w)
        wi *= (1 - pd);
    std::vector<Eigen::MatrixXd> m = x_predict.m;
    std::vector<Eigen::MatrixXd> P = x_predict.P;

    // 对真实目标更新
    for (int s = 0; s < Y.n; s++)
    {
        std::vector<double> w1;
        for (int j = 0; j < x_predict.J; j++)
        {
            Eigen::VectorXd y = H * x_predict.m[j];
            Eigen::MatrixXd S = R + H * x_predict.P[j] * H.transpose();
            Eigen::VectorXd diff = Y.m.col(s) - y;
            double e = std::exp(-0.5 * diff.transpose() * S.inverse() * diff);
            w1.push_back(pd * x_predict.w[j] * e);

            Eigen::MatrixXd K = x_predict.P[j] * H.transpose() * S.inverse();
            Eigen::VectorXd m1 = x_predict.m[j] + K * diff;
            m.push_back(m1);
            Eigen::MatrixXd P1 = x_predict.P[j] - K * H * x_predict.P[j];
            P.push_back(P1);
            //cout << P1 << endl;
        }
        double K = r / std::pow(200, 2); // 杂波强度
        double w2 = std::accumulate(w1.begin(), w1.end(), 0.0);
        for (auto& w1i : w1)
            w1i /= (K + w2);
        w.insert(w.end(), w1.begin(), w1.end());
    }
    State x_filter;
    x_filter.m = m;
    x_filter.P = P;
    x_filter.w = w;
    x_filter.J = (1 + Y.n) * x_predict.J;
    return x_filter;
}

State PRUN_MERG(State x_filter, double T_prun, double U_merg, int J_max)
{
    State I;
    I.J = 0;
    std::vector<Eigen::MatrixXd> m;
    std::vector<Eigen::MatrixXd> P;
    std::vector<double> w;

    // 修剪
    for (int i = 0; i < x_filter.J; i++)
    {
        if (x_filter.w[i] > T_prun)
        {
            I.m.push_back(x_filter.m[i]);
            I.P.push_back(x_filter.P[i]);
            I.w.push_back(x_filter.w[i]);
            I.J++;
        }
    }

    // 合并
    int l = 0;
    while (I.J > 0)
    {
        l++;
        auto max_w = std::max_element(I.w.begin(), I.w.end());
        int j = std::distance(I.w.begin(), max_w);
        State L;
        L.J = 0;
        std::vector<int> L_b;
        for (int i = 0; i < I.J; i++)
        {
            Eigen::MatrixXd diff = (I.m[i] - I.m[j]);
            //cout<<diff<<endl;
            // TODO 检查这里的计算是否正确
            Eigen::MatrixXd merg = diff.transpose() * I.P[i].inverse() * diff;
            //cout<<merg(0,0)<<endl;
            if (merg(0, 0) <= U_merg)
            {
                L.m.push_back(I.m[i]);
                L.P.push_back(I.P[i]);
                L.w.push_back(I.w[i]);
                L.J++;
                L_b.push_back(i);
            }
        }

        // 剔除合并项
        for (int i = 0; i < I.J; i++) {
            if (std::find(L_b.begin(), L_b.end(), i) == L_b.end()) {
                I.m.push_back(I.m[i]);
                I.P.push_back(I.P[i]);
                I.w.push_back(I.w[i]);
            }
        }
        I.m.erase(I.m.begin(), I.m.begin() + I.J);
        I.P.erase(I.P.begin(), I.P.begin() + I.J);
        I.w.erase(I.w.begin(), I.w.begin() + I.J);
        I.J -= L.J;

        // 合并
        w.push_back(std::accumulate(L.w.begin(), L.w.end(), 0.0));
        Eigen::MatrixXd m1 = Eigen::MatrixXd::Zero(6, 1);
        for (int i = 0; i < L.J; i++)
            m1 += L.w[i] * L.m[i];
        m.push_back(m1 / w[l - 1]);
        /*cout<<m[l-1].transpose()<< endl;
        cout<<w[l-1]<<endl;*/
        Eigen::MatrixXd P1 = Eigen::MatrixXd::Zero(6, 6);
        for (int i = 0; i < L.J; i++)
            P1 += L.w[i] * (L.P[i] + (m[l - 1] - L.m[i]) * (m[l - 1] - L.m[i]).transpose());
        //cout << P1 << endl;
        P.push_back(P1 / w[l - 1]);
    };

    // 判断高斯分量个数是否超过上限
    State x_prun;
    if (l > J_max)
    {
        auto max_w = std::max_element(w.begin(), w.end());
        int j = std::distance(w.begin(), max_w);
        for (int i = 0; i < J_max; i++)
        {
            x_prun.m.push_back(m[j]);
            x_prun.P.push_back(P[j]);
            x_prun.w.push_back(*max_w);
        }
        x_prun.J = J_max;
    }
    else
    {
        x_prun.m = m;
        x_prun.P = P;
        x_prun.w = w;
        x_prun.J = l;
        
    }
    return x_prun;
};

State MERG(State x_filter, double TT_prun, double UU_merg, int J_max)
{
    State I;
    I.J = 0;
    std::vector<Eigen::MatrixXd> m;
    std::vector<Eigen::MatrixXd> P;
    std::vector<double> w;
    // 修剪
    for (int i = 0; i < x_filter.J; i++)
    {
        if (x_filter.w[i] > TT_prun)
        {
            I.m.push_back(x_filter.m[i]);
            I.P.push_back(x_filter.P[i]);
            I.w.push_back(x_filter.w[i]);
            I.J++;
        }
    }

    // 合并
    int l = 0;
    while (I.J > 0)
    {
        l++;
        int j = std::distance(I.w.begin(), std::max_element(I.w.begin(), I.w.end()));
        State L;
        L.J = 0;
        std::vector<int> L_b;

        for (int i = 0; i < I.J; i++)
        {
            Eigen::VectorXd diff = I.m[i] - I.m[j];
           
            Eigen::MatrixXd P=(I.P[i]+I.P[j]).inverse();
           
            double merg = diff.transpose() * (I.P[i] + I.P[j]).inverse() * diff;
           
            if (merg <= UU_merg)
            {
                L.m.push_back(I.m[i]);
                L.P.push_back(I.P[i]);
                L.w.push_back(I.w[i]);
                L.J++;
                L_b.push_back(i);
            }
        }

        // 剔除合并项
        for (int i = 0; i < I.J; i++) {
            if (std::find(L_b.begin(), L_b.end(), i) == L_b.end()) {
                I.m.push_back(I.m[i]);
                I.P.push_back(I.P[i]);
                I.w.push_back(I.w[i]);
            }
        }
        I.m.erase(I.m.begin(), I.m.begin() + I.J);
        I.P.erase(I.P.begin(), I.P.begin() + I.J);
        I.w.erase(I.w.begin(), I.w.begin() + I.J);
        I.J -= L.J;

        // 合并
        // TODO: 这里的计算有问题
        double w1 = std::accumulate(L.w.begin(), L.w.end(), 0.0);
        double w_l = 1;
        for (int kkk = 0; kkk < L.J; kkk++) {
            w_l *= 1 - L.w[kkk];
        }
        w.push_back(1-w_l);
        Eigen::VectorXd m1 = Eigen::VectorXd::Zero(6);
        for (int i = 0; i < L.J; i++)
        {
            m1 += L.w[i] * L.m[i];
        }
        m.push_back(m1 / w1);
        /*cout << m[l - 1].transpose() << endl;
        cout << w[l-1] << endl;*/
        Eigen::MatrixXd P1 = Eigen::MatrixXd::Zero(6, 6);
        for (int i = 0; i < L.J; i++)
        {
            P1 += L.w[i] * (L.P[i] + (m[l-1] - L.m[i]) * (m[l-1] - L.m[i]).transpose());
        }
        P.push_back(P1 / w1);
    }

    // 判断高斯分量个数是否超过上限
 /*   State x_merg;
    if (l > J_max)
    {
        auto max_w = std::max_element(I.w.begin(), I.w.end());
        int j = std::distance(I.w.begin(), max_w);
        for (int i = 0; i < J_max; i++)
        {
            x_merg.m.push_back(I.m[j]);
            x_merg.P.push_back(I.P[j]);
            x_merg.w.push_back(I.w[j]);
        }
        x_merg.J = J_max;
    }
    else
    {
        x_merg = I;
        x_merg.J= l;
    }

    return x_merg;*/

    State x_prun;
    if (l > J_max)
    {
        auto max_w = std::max_element(w.begin(), w.end());
        int j = std::distance(w.begin(), max_w);
        for (int i = 0; i < J_max; i++)
        {
            x_prun.m.push_back(m[j]);
            x_prun.P.push_back(P[j]);
            x_prun.w.push_back(*max_w);
        }
        x_prun.J = J_max;
    }
    else
    {
        x_prun.m = m;
        x_prun.P = P;
        x_prun.w = w;
        x_prun.J = l;
    }
    return x_prun;
};

std::vector<Eigen::MatrixXd> ESTIMATE(State x_filter)
{
    std::vector<Eigen::MatrixXd> x_estimate;
    for (int i = 0; i < x_filter.J; i++)
    {
        if (x_filter.w[i] > 0.5)
        {
            for (int j = 0; j < round(x_filter.w[i]); j++)
            {

                x_estimate.push_back(x_filter.m[i]);
            }
        }
    }
    return x_estimate;
};

int main()
{
    //*******************初始化设置***********************//
    //**************************************************//
    int T = 1;         // 采样时间间隔
    const int N = 100; // 测量总时间
    int TARGET_NUM = 1;

    Eigen::VectorXd x_number(N); // 每一次测量时目标的数量
    x_number(0) = 1;

    double q_noise1 = 0.5; // 状态噪声方差
    double q_noise2 = 0.1; // 状态噪声方差
    Eigen::VectorXd q_noise(3);
    q_noise << q_noise1, q_noise2, q_noise2;

    Eigen::MatrixXd F(6, 6);
    F << 1.0, T, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0,
        0, 0, 1, T, 0, 0,
        0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 1, T,
        0, 0, 0, 0, 0, 1.0;
   
    Eigen::MatrixXd G(6, 3);
    G << T * T / 2.0, 0.0, 0,
        T, 0, 0,
        0, T* T / 2.0, 0,
        0, T, 0,
        0, 0, T* T / 2.0,
        0, 0, T;
  
    Eigen::MatrixXi Cons(4, 4);
    Cons << 1, 1, 1, 0,
        1, 1, 0, 1,
        1, 0, 1, 1,
        0, 1, 1, 1;

    Real_State groundTruth = TARGET_FORM(F, G, q_noise);
    //******************存活目标初始化*****************//
    State initial;
    for (int i = 0; i < N; i++) {
        initial.m.push_back(groundTruth.m.s[0].col(i));
    }
    for (int i = 0; i < TARGET_NUM; i++)
    {
        initial.w.push_back(1);
    }
    Eigen::VectorXd v(6);
    v << 1, 2, 1, 2, 1, 2;
    initial.P.push_back(v.asDiagonal());
    initial.J = 1;

    //*****************新生目标初始化******************//
    State birth;
    Eigen::VectorXd vector6d(6);
    vector6d << -200, 1, 200, -1, 0, 0;
    Eigen::MatrixXd matrix(6, 1);
    for (int i = 0; i < 6; i++)
    {
        matrix(i, 0) = vector6d(i);
    }
    birth.m.push_back(matrix);

    vector6d << 200, -1, -200, 1, 0, 0;
    for (int i = 0; i < 6; i++)
    {
        matrix(i, 0) = vector6d(i);
    }
    
    birth.m.push_back(matrix);

    v << 1, 2, 1, 2, 1, 1;
    birth.P.push_back(v.asDiagonal());
    birth.P.push_back(v.asDiagonal());
    birth.w = { 0.8, 0.8 };
    birth.J = 2;

    //***************生成量测报告*********************//
    // 创建测量矩阵H
    Eigen::MatrixXd H = Eigen::MatrixXd::Zero(3, 6);
    H(0, 0) = 1;
    H(1, 2) = 1;
    H(2, 4) = 1;

    // 创建测量噪声标准差向量r_noise
    Eigen::Vector3d r_noise;
    r_noise << 0.5, 0.5, 0.5;

    // 创建测量噪声协方差矩阵R
    Eigen::MatrixXd R = r_noise.array().square().matrix().asDiagonal();

    // 创建目标存活概率和目标检测概率
    double ps = 0.99;
    double pd = 0.98;

    // 创建每时刻杂波点平均数
    int r = 3;

    // 创建蒙特卡罗次数
    int motocaro_number = 2;

    // 创建PHD估计的目标数目和事件触发后估计的目标数目
    // Eigen::MatrixXd x_number = Eigen::MatrixXd::Zero(motocaro_number, N);
    Eigen::MatrixXd x_number_e = Eigen::MatrixXd::Zero(motocaro_number, N);

    // 创建RMSE
    // Eigen::MatrixXd d_rmse1 = Eigen::MatrixXd::Zero(motocaro_number, N);
    Eigen::MatrixXd d_rmse = Eigen::MatrixXd::Zero(motocaro_number, N);

    // 创建事件触发每个时刻的触发情况
    Eigen::MatrixXd eSum = Eigen::MatrixXd::Zero(motocaro_number, N); //[moto_num,measure_num]

    std::vector<std::vector<std::vector<Eigen::MatrixXd>>> epose(motocaro_number, std::vector<std::vector<Eigen::MatrixXd>>(N));

    // 创建未加事件触发前的计算数目、事件触发节省的计算数目和事件触发后的计算数目
    int Sum1 = 0;
    int Save = 0;
    int Sum2 = 0;

    //********************开始进行蒙特卡罗仿真*************************//
    for (int kk = 0; kk < motocaro_number; kk++)
    {
        // 1. 产生4个传感器的测量数据
        int number_sensor = Cons.rows();
        std::vector<std::vector<Measurement>> Y_measure; // [sensor_num,measure_num]
        for (int i = 0; i < number_sensor; i++)
        {
            std::vector<Measurement> Y = MEASUREMENT(H, r_noise, groundTruth, N, pd);
            Y_measure.push_back(Y);
        }

        // 2. 构建目标随机集
        std::vector<RandomState> x;
        for (int i = 0; i < N; i++)
        {
            RandomState rs;
            for (int j = 0; j < TARGET_NUM; j++)
            {
                Eigen::MatrixXd m(6, 1);
                m = groundTruth.m.s[j].col(i);
                // for (int k = 0; k < 6; k++)
                // {
                //     m(k, 0) = groundTruth.m.s[j](k, i);
                // }
                rs.m.push_back(m);
            }

            x.push_back(rs);
        }

        // 3. 开始滤波
        double T_prun = 1e-7;  // 修剪门限
        double TT_prun = 1e-3; // 融合时的修剪门限
        double U_merg = 1;     // 合并门限
        double UU_merg = 1000;   // 融合时的合并门限
        int J_max = 300;       // 最大高斯数
        int JJ_max = 300;      // 融合时的最大高斯数

        // 计算传感器数目
        FilterPHD x_filter_phd;
        for (int i = 0; i < number_sensor; i++)
        {
            x_filter_phd.a.push_back(initial);
        }

        Eigen::VectorXd j(6);
        j << 0, 0, 0, 0, 0, 0;
        for (int i = 1; i < N; i++)
        {
            for (int L = 0; L < number_sensor; L++)
            {
                int u = Y_measure[L][i].n - Y_measure[L][j[L]].m.cols();
                Eigen::MatrixXd Q = Eigen::MatrixXd::Zero(3, std::abs(u));
                if (u > 0)
                {
                    Y_measure[L][j[L]].m.conservativeResize(Eigen::NoChange, Y_measure[L][j[L]].m.cols() + Q.cols());
                    Y_measure[L][j[L]].m.rightCols(Q.cols()) = Q;
                }
                else if (u < 0)
                {
                    Y_measure[L][i].m.conservativeResize(Eigen::NoChange, Y_measure[L][i].m.cols() + Q.cols());
                    Y_measure[L][i].m.rightCols(Q.cols()) = Q;
                }
               
                Y_measure[L][i].c = Y_measure[L][i].m - Y_measure[L][j[L]].m;
                Y_measure[L][i].v = Y_measure[L][i].c * Y_measure[L][i].c.transpose();
                
                
                if (Y_measure[L][i].v(0, 0) / 1e5 > 8 && Y_measure[L][i].v(1, 1) / 1e5 > 8)
                {
                    Sum2++;
                    eSum(kk, i) += 0.25;
                    T = 0;
                    j[L] = i;
                    State x_predict = PREDICT(x_filter_phd.a[L], F, ps, birth, q_noise, G);

                    auto x_update=UPDATE(x_predict, Y_measure[L][i], pd, H, R, r);
                    
                    x_filter_phd.a[L] = x_update;
                    
                }
                else
                {
                    Save++;
                    eSum(kk, i) += 0;
                    T++;
                    State x_predict = PREDICT(x_filter_phd.a[L], F, ps, birth, q_noise, G);
                    x_filter_phd.a[L] = UPDATE(x_predict, Y_measure[L][j[L]], pd, H, R, r);
                }
                x_filter_phd.a[L] = PRUN_MERG(x_filter_phd.a[L], T_prun, U_merg, J_max);
                int ccc = 1;
            }

            FilterPHD x_filter_phde(x_filter_phd);
            // 传感器融合
            for (int C = 0; C < 1; C++)
            {
                std::vector<State> I_a(number_sensor);
                for (int s = 0; s < number_sensor; s++)
                {
                    I_a[s].m.clear();
                    I_a[s].P.clear();
                    I_a[s].w.clear();
                    I_a[s].J = 0;
                }

                for (int p = 0; p < number_sensor; p++)
                {
                    for (int q = 0; q < number_sensor; q++)
                    {
                        if (Cons(p, q) == 1)
                        {
                            if (x_filter_phde.a[q].J != 0)
                            {
                                I_a[p].m.insert(I_a[p].m.end(), x_filter_phde.a[q].m.begin(), x_filter_phde.a[q].m.end());
                                I_a[p].P.insert(I_a[p].P.end(), x_filter_phde.a[q].P.begin(), x_filter_phde.a[q].P.end());
                                I_a[p].w.insert(I_a[p].w.end(), x_filter_phde.a[q].w.begin(), x_filter_phde.a[q].w.end());
                                I_a[p].J += x_filter_phde.a[q].J;
                            }
                        }
                    }
                }

                // 传感器融合后合并
                for (int p = 0; p < number_sensor; p++)
                {
                    x_filter_phde.a[p] = MERG(I_a[p], TT_prun, UU_merg, J_max); // 合并高斯项
                }
            }
            double error_total=0;
            for (int p = 0; p < number_sensor; p++)
            {
                double a= (1.0 / number_sensor) * round(std::accumulate(x_filter_phde.a[p].w.begin(), x_filter_phde.a[p].w.end(), 0.0));
                x_number_e(kk, i) += a; // 估计目标数目
                std::vector<Eigen::MatrixXd> x_estimate = ESTIMATE(x_filter_phde.a[p]);                                                            // 目标状态估计
                
                epose[kk][i] = x_estimate;                                                                                                        // 存储估计的状态
                if (!x[i].m.empty() && x_estimate.size() != 0)                                                                                    // 误差分析
                {
                    Eigen::MatrixXd d = (x[i].m[0] - x_estimate[0]).transpose() * (x[i].m[0] - x_estimate[0]);
                    double error = d(0, 0);
                    error_total += error;
                    d_rmse(kk, i) += d(0, 0);
                }
            }
            cout<< "第" << i << "次测量" << "  误差为： " << sqrt(error_total) << endl;
        }
    }

    //******************计算RMSE*********************//
    // 通信时刻
    double eSum_mean = eSum.sum() / (motocaro_number * N);
    // 构建一个[1,N]的全1矩阵sss
    Eigen::MatrixXd sss = Eigen::MatrixXd::Ones(1, N);
    sss.setConstant(eSum_mean);

    // 目标估计数目
    Eigen::MatrixXd x_number_e_mean = x_number_e.colwise().mean();

    // RMSE
    Eigen::MatrixXd d_rmse_mean = d_rmse.colwise().mean(); // 每一列的均值
    // 开方
    d_rmse_mean = d_rmse_mean.array().sqrt() - 1.0;

    // 将d_rmse_mean存储到文件中
    std::ofstream file("d_rmse_mean.txt");
    if (file.is_open())
    {
        for (int k = 0; k < d_rmse_mean.cols(); k++) {
            file << d_rmse_mean(0, k) << std::endl;
        }
        
        file.close();
    }
    else
    {
        std::cout << "文件打开失败" << std::endl;
    }

    return 0;
}