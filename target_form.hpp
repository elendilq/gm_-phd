#pragma once
#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <vector>

struct M // matlab就这么写的
{
    std::vector<Eigen::MatrixXd> s; // 目标状态 [target_num,6,mea_num]
};

class Real_State
{
public:
    M m;               // 用于存储目标位置 m.s [target_num,6,mea_num]
    Eigen::MatrixXd n; // 用于存储噪声 [target_num,mea_num]
    Real_State(int target_num, int measure_num)
    {
        for (int i = 0; i < target_num; i++)
        {
            Eigen::MatrixXd matrix(6, measure_num);
            matrix.setConstant(0.0);
            m.s.push_back(matrix);
        }

        n.resize(target_num, measure_num);
        n.setConstant(1.0);
    }
};
